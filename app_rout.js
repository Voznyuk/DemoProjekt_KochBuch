var express = require('express');
var path = require('path');
var fs = require('fs');
var favicon = require('serve-favicon');
//  This tells express to log via morgan. Provides flexible request logging.
var logger = require('morgan');
// Parses cookies from web browsers into req.cookies
var cookieParser = require('cookie-parser');
// Provides req.body and req.files for subsequent middleware  to use.
// Consumes and parses the request body into req.body
var bodyParser = require('body-parser');
var flash = require('express-flash');
// We'll be using Passport for authentification
var passport = require('passport');
// We'll be using Mongoose for work with  DB
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
// Manages session data
var session = require('express-session');
// My config  port/DB url
var port=require('./config/database').port;
var url =require('./config/database').url;
// 
var app = express();

// I’ll use EJS templates, which are similar in structure to  HTML. 
// set up EJS view engine and path ...
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//developer Help
// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});
// setup the logger
app.use(logger('combined', {stream: accessLogStream}));

// The express "body parser" gives us the parameters of a
// POST request is a convenient req.body object
//app.use(bodyParser());
//app.use(bodyParser.urlencoded());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use(cookieParser());

// Serve static file logo.jpg  in this folder
app.use(express.static(path.join(__dirname, 'public')));

//app.use(session({ secret: 'secret' }));

app.use(session({secret:'secret',
        resave: true,
        saveUninitialized: true
    })
);

//passport middlewares which process the incoming requests before handling them down to the routes
app.use(passport.initialize());
app.use(passport.session());

 app.use(flash());
// Now every view will have access to any find messages that I flash.

//  I separate routes into their own file rout.js. routes shall use Express and I pass the app var in as a parameter to a method
var routes = require('./routes/rout')(app);

//  catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
     error: {}
     // , if you do not  want information about error
    // error:err
  });
});


// connect to MongoDB !
var db =mongoose.connect(url);
mongoose.connection.on('open', function() {
  console.log('Connected to Mongoose');
  //Server start only if DB connection is OK
 // Express 4.x , now treats app.listen() as an asynchronous operation. I use this way Express 4,
  // because I do not  want to reuse the HTTP server, for example to run socket.io within the same HTTP server instance
 var server= app.listen(port, function () {
    console.log('Listening on http://localhost:' + (port))
  });
});
// If something goes wrong with DB connection, call the callback with the error
mongoose.connection.on('error', function () {
  console.log("DB connection error:", err.message);
  console.log("Start mongod.exe and mongo.exe in 2 Terminals!!!!!!!!!");
});


module.exports = app;

