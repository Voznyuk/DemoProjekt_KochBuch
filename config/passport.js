/**
 * Created by nata on 26.12.2016.
 */
// I used code : https://github.com/danielgynn/express-authentication.git`
var LocalStrategy = require('passport-local').Strategy;
var User =require('../models/User');
module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true

        },
        function(req, email, password, done) {
              process.nextTick(function() {
                User.findOne({ 'local.email':  email }, function(err, user) {
                    console.log(user);
                    if (err)
                        return done(err);
                    if (!user) {
                        var newUser = new User();
                        newUser.name=email;
                        newUser.status='aktiv';
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                            });
                    }

                    else {
                         return done(null, false, req.flash('signupMessage', 'That email is already taken.'));

                    }
                });
             });
        //
        }));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, email, password, done) {
            User. findOneAndUpdate({ 'local.email':  email  },{$set: {status : 'aktiv'}}, function(err, user) {
                if (err)
                    return done(err);
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Sorry! Wrong password.'));
                return done(null, user);
            });
        }));



};
