/**
  * Created by nata on 02.01.2017.
 */
var async=require('async');
var passport = require('passport');
var mongoose =require('mongoose');
var Rezept =require('../../models/rezept');
var Bewertung =require('../../models/bewertung');
require('../../config/passport')(passport);
var typ=require('../../config/gerichtTyp').typ;
var findMessag="";
var User=require('../../models/User');
var authError= require ('../../config/error').auth;
var findInDatabank=require ('../../config/error').findInDatabank;

//ADD REZEPT
exports.addRezept=function(req, res,next)  {
    if(!req.user ||req.user.status=='deaktiv')   return next(authError);
    // var user=req.user;
    if (req.body) {
          var    rezept = {
            rezeptName: req.body.rezeptName,
            user: req.body.user,
            rezeptTyp: req.body.rezeptTyp,
            kochZeit: req.body.kochZeit,
            rezeptBeschreibung: req.body.rezeptBeschreibung,
            rezeptIngradients: 'ingradientsArray BODY'
        };

            }

    else {
        rezept=new Rezept()
    }
    rezeptIngradients="TEST";
    var  user =req.user;
    res.render('FormAddRezept', {
        message: req.flash('errorMessageSave'),
        user:user,
        typ:typ,
        rezept:rezept,
        rezeptIngradients:rezeptIngradients})

};
//FIND REZEPT
exports.findRezeptForm=function(req, res,next) {
    if(!req.user ||req.user.status=='deaktiv')   return next(authError);
        res.render('FormfindRezept',{
        user: req.user,
        typ:typ});
    };


//_List of all rezepts
exports.rezeptList =function(req, res, next) {
    if(!req.user||req.user.status=='deaktiv')   return next(authError);
      Rezept.find({},null, {sort: '-updated_at'},function (err, rezepts) {
        if (err) return next(findInDatabank);
        if (!rezepts.length)  req.flash('findListMessages',"No  Rezepts ");
        res.render('listAllRezepts', {
           
            title: 'RezeptList',
            message:req.flash('findListMessages'),
            rezepts: rezepts,
            user: req.user,typ:typ
        })
      })
};



//_Choose rezept from list
exports.listId= function(req, res, next) {
    if(!req.user ||req.user.status=='deaktiv')   return next(authError);
          Rezept.findOne({'_id': req.params.id}, function (err, rezept) {
        if (err) return next(findInDatabank);
        res.render('view.ejs', {
         
            title: 'Rezept view',
            rezept: rezept,
            user:req.user,
            typ:typ
        })

    });
};

//_ List of Rezept's  Feedbacks

exports.rezeptListBewertungen =function(req, res, next) {
    var user=req.user;
    async.waterfall([
            function readRezept(callback) {
                Rezept.findOne({'_id': req.params.id}, function (err, rezept) {

                    if (err) return next(findInDatabank);
                    callback(err, rezept)
                });
            },

            function readBewertung(rezept, callback) {
                    Bewertung.find({'rezeptName': rezept.rezeptName}, function (err, bewertungs) {
                    if (err) return next(findInDatabank);
                    if (!bewertungs.length)  req.flash('BewertungFind','Any evaluation of this rezepts');
                    callback(err, bewertungs, rezept)
                })
            }],
        function (err,bewertungs,rezept) {
            res.render('listRezeptsBewertungen', {
                title: 'BewertungsList',
                message:req.flash('BewertungFind'),
                rezept:rezept,
                bewertungs:bewertungs,
                typ:typ,
                user:user
            })

        }
    )
}

/* DELETE  */
exports.delete= function(req, res,next ) {
    if(!req.user ||req.user.status=='deaktiv')   return next(authError);
          Rezept.remove({'_id':  req.params.id},  function (err) {
        if (!err) {
            res.redirect('/rezeptList')}  else
                    return next(findInDatabank)
    });
};


//_Rezepts of user
exports.userId= function(req, res, next) {
    if(!req.user ||req.user.status=='deaktiv')   return next(authError);
                Rezept.find({'user': req.params.name}, function (err, rezept) {
                if (err) return next(findInDatabank);
                if (!rezept.length)    req.flash('findUsersRezept', "Any rezept of this User ");
                res.render('listAllRezepts.ejs', {
                    title: "UserRezepts",
                    
                    message:req.flash('findUsersRezept'),
                    rezepts: rezept,
                    user:req.user,
                    typ:typ
                })
            });
};

// Edit Form
exports.editForm=function(req, res,next) {
    if(!req.user ||req.user.status=='deaktiv')   return next(authError);
           Rezept.findOne({'_id' : req.params.id}, function(err, rezept) {
            if(err)
               return next(findInDatabank);
            else
                var ingradientsArray=rezept.rezeptIngradients
            ingradient=[]
            for (i=0; i<ingradientsArray.length; i++){
                ingradient.push(ingradientsArray[i].ingradientsName)
            }
            var ingradients=ingradient.join(',')
            res.render('FormEditRezept',{
                rezept: rezept,
                user:req.user,
                typ:typ,
                ingradient: ingradients,
                 message: req.flash('errorEditMessage')
             } );

         });
};