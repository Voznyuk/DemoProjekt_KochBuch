/**
 * Created by nata on 03.01.2017.
 */
var passport = require('passport');
var Rezept =require('../../models/rezept');
var Bewertung =require('../../models/bewertung');
require('../../config/passport')(passport);
var typ=require('../../config/gerichtTyp').typ;
var User=require('../../models/User');
var findInDatabank= require ('../../config/error').findInDatabank;
var saveError= require ('../../config/error').saveError;

 //ADD in DB
exports.addRezept= function(req, res) {
    var ingradientsArray = [];
    var rezeptIngradients = req.body.rezeptIngradients.split(', ');
    for (var i = 0; i < rezeptIngradients.length; i++) {
        var ingradient = {ingradientsName: rezeptIngradients[i]};
        ingradientsArray.push(ingradient);
    }

    // var user=req.user
    var rezept = {
        rezeptName: req.body.rezeptName,
        updated_at: Date.now(),
        user: req.user.name,
        rezeptTyp: req.body.rezeptTyp,
        kochZeit: req.body.kochZeit,
        rezeptBeschreibung: req.body.rezeptBeschreibung,
        rezeptIngradients: ingradientsArray
    };


    var newRezept = new Rezept(rezept);
       newRezept.save(function (err) {
        if (err) {

            ingradient=[];
            for (i=0; i<ingradientsArray.length; i++){
                ingradient.push(ingradientsArray[i].ingradientsName)

            }
            var ingradients=ingradient.join(',');

            req.flash('errorMessageSave', " Save error: Rezept name or same describtion  alredy exist");
            res.render('FormAddRezept', {
                message: req.flash('errorMessageSave'),
                user:req.user,
                typ:typ,
                rezept:newRezept, rezeptIngradients:ingradients
            })
        }


        else {
             res.redirect('/profile');
        }

    })
};



//Rezept Bewerung
exports.bewertung= function(req,res, next) {
    var bewertung= {
            rezeptName: req.body.rezeptName,
            user: req.body.user,
            rezeptBewertung: req.body.rezeptBewertung
        };
    var newBewertung = new Bewertung(bewertung);
        newBewertung.save(function (err) {
            if (err) return next(saveError);
            res.redirect('/profile');
        });
};

// Rezepts update
exports.update= function(req,res) {
     id = req.params.id;
     var ingradientsArray=[];
     var rezeptIngradient=req.body.rezeptIngradients.split(',');
         rezeptIngradient.forEach(function (item) {
            ingradient={ingradientsName:item};

             ingradientsArray.push(ingradient)
         });

    var rezept = {
        rezeptName : req.body.rezeptName,
        kochZeit : req.body.kochZeit,
        rezeptBeschreibung: req.body.rezeptBeschreibung,
        rezeptIngradient:ingradientsArray,
        id:id

    };
    ingradient=[];

    for (i=0; i<ingradientsArray.length; i++){
        ingradient.push(ingradientsArray[i].ingradientsName)
    }
    var ingradients=ingradient.join(',');
    var ingradient= ingradients;
    // console.log("help1")

    Rezept.findByIdAndUpdate(id, rezept, function(err,rezep) {
              if (err){
                req.flash('errorEditMessage', "Save error: Rezept name or same describtion  alredy exist");
                 ingradient=[];

                    for (i=0; i<ingradientsArray.length; i++){
                        ingradient.push(ingradientsArray[i].ingradientsName)
                    }
                var ingradients=ingradient.join(',');

                res.render('FormEditRezept',{
                    rezept: rezept,
                    user:req.user,
                    typ:typ,
                    ingradient: ingradients,
                    message: req.flash('errorEditMessage')
                } )

            }
            // res.send('Problem occured with update' + err)

            else {
                res.render('infoAfterEdit', { rezept: rezept, user: req.user, typ:typ, rezeptTyp:rezep.rezeptTyp, ingradient:ingradient})
            }
    });
};

//_FIND REZEPT with param.
exports.findRezept= function(req,res,next) {
    var typ1 = req.body.rezeptTyp;
    var user = req.body.user;
    var zeit = req.body.kochZeit;
    Rezept.find({'rezeptTyp':typ1 , user:  user, 'kochZeit': { $gt: 0, $lt: zeit }},function (err, rezepts) {
        if (err) return next(findInDatabank);
        if (rezepts.length<=0) req.flash('findUserMessage','any rezepts with this parameters');
        res.render('listAllRezepts', {
            
            title: 'Rezept view',
             message: req.flash('findUserMessage'),
             rezepts: rezepts,
             user: req.user,typ:typ
        })
    });
};

//_Find all rezepts of user
exports.findUserRezept= function(req,res,next) {
       var user = req.body.user;
        Rezept.find({ 'user':user},function (err, rezepts) {
            if (err) return next(findInDatabank);
            if (!rezepts.length)    req.flash('findUsersRezept', "Any  Rezepts of this  User ");
            res.render('listAllRezepts', {
               
                title: 'Rezept view',
                message: req.flash('findUsersRezept'),
                user: req.user,
                rezepts: rezepts,
                typ:typ
            })
        });
};

//_ Find all rezepts of Typ
exports.findTypRezept= function(req,res,next) {
    Rezept.find(  {'rezeptTyp': req.body.rezeptTyp},function (err, rezepts) {
        if (err) return next(findInDatabank);
        if (!rezepts.length)  req.flash('findTypRezept', "Any  Rezepts of this  Typ ");
              res.render('listAllRezepts',{
                 
                  title: 'Rezept view',
                  message:  req.flash('findTypRezept'),
                  user: req.user,
                  rezepts: rezepts,
                  typ: typ
              })
    });
};

