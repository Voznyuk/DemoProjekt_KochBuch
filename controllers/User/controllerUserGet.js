 var passport = require('passport');
var User =require('../../models/User');
var Rezept =require('../../models/rezept');
require('../../config/passport.js')(passport);
var typ=require('../../config/gerichtTyp').typ;
var logout= require ('../../config/error').logout;
var findInDatabank= require ('../../config/error').findInDatabank;

//start
exports.start=function (req, res, next) {
    res.render('index');
};

//login
exports.login=function (req, res) {
    res.render('login.ejs',
        {
            message: req.flash('loginMessage'),
             title: "Login", //page title
            action: "/login", //post action for the form
            fields: [
                {name: 'email', type: 'text', property: 'required'},   //first field for the form
                {name: 'password', type: 'password', property: 'required'}  //another field for the form

            ]
        });
};

//welcom home page
exports.profile=function (req, res,next) {
     var fiveTagAgo = new Date();
       fiveTagAgo.setDate(fiveTagAgo.getDate() - 5);
            Rezept.find({updated_at: { $gt: fiveTagAgo}}, null, {sort: '-updated_at'},function(err, rezepts){
                if (err) return next(findInDatabank);
                if (!rezepts.length)  req.flash('findMessages',"No new Rezepts during last 5 days");
              res.render('listNewRezeptsHome.ejs', {
                 
                  message: req.flash('findMessages'),
                  rezepts: rezepts,
                  user: req.user,
                  typ:typ
              })
            })
};

//List of users
exports.userAktivList =function(req, res, next) {
       User.find({},null,{sort:'status'},function (err, users) {
        if (err) req.flash('errorfindMessages',"Find Error !");
        if (!users.length)  req.flash('findUsers',"No Users");
        res.render('listAllUsers.ejs', {
            title: "All users list",
            errorMessage:req.flash('errorfindMessages'),
            message: req.flash('findUsers'),
            users: users,
            user: req.user,
            typ:typ
        })

    })
};

//logout
exports.logout=function (req, res,next) {
    User.update({'_id': req.user.id}, {$set: {status : 'deaktiv'}},
    function(err) {
        if (err)
            return next(logout);
        else {
        req.logout();}
    });
    res.redirect('/');
};

//signup
exports.signup=function (req, res) {
    res.render('signup.ejs', {
        message: req.flash('signupMessage'),
        title: "Signup", //page title
        action: "/signup", //post action for the form
        fields: [
            {name: 'email', type: 'text', property: 'required'},   //first field for the form
            {name: 'password', type: 'password', property: 'required'}   //another field for the form
        ]
    });
};


