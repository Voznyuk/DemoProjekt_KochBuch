/**
 * Created by nata on 03.01.2017.
 */
var passport = require('passport');
var Rezept =require('../../models/rezept');
require('../../config/passport')(passport);


exports.login=passport.authenticate('local-login', {
    successRedirect: '/profile',
    failureRedirect: '/login',
    failureFlash: true

});

exports.signup=passport.authenticate('local-signup', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    failureFlash: true
});



