/**
 * Created by nata on 26.12.2016.
 */
var mongoose = require('mongoose');
// var db = require('../dba').db;
var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema
    ,ObjectId = Schema.ObjectId

var userSchema = new Schema({
    local: {
        name: String,
        email: String,
        password: String
    },
    name:String,
    status:String
});

userSchema.methods.generateHash = function(password) {
    // After the salt is generated, bcrypt.hash() is called, which hashes the .pass property
    // and the salt.Generate a  8-character salt
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);
