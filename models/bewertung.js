/**
 * Created by nata on 10.01.2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema
    ,ObjectId = Schema.ObjectId;

var Bewertung = new Schema({
    rezeptName: {
        type: String
            },
    rezeptBewertung: {
        type: String
           },
    user: String

});

module.exports = mongoose.model('Bewertung', Bewertung);
