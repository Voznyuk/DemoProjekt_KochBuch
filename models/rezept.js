 // * Created by nata on 17.12.2016.
 // */

var mongoose = require('mongoose');
 var Schema = mongoose.Schema
    ,ObjectId = Schema.ObjectId;

var Rezept = new Schema({
    rezeptName: {
        type: String,
        unique: true,
        required: true
    },
    rezeptBeschreibung: {
        type: String,
        required: true
    },
    user: String,
    kochZeit: String,
    rezeptTyp: String,
    updated_at : {type: Date, default: Date.now},
    rezeptIngradients: [{ingradientsName:String, ingradientsMenge:Number}]
//    Für weitere Entwicklung-  ingradientsMenge:Number
});

module.exports = mongoose.model('Rezept', Rezept)
