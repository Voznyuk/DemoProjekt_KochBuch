/**
 * Created by nata on 02.01.2017.
 */
var passport = require('passport');
var mongoose =require('mongoose');
// var app = require('../app_rout.js');
var Rezept =require('../models/rezept');
var controllerStart=require('../controllers/index');
var controllerUserGet = require('../controllers/User/controllerUserGet');
var controllerUserPost = require('../controllers/User/controllerUserPost');
var controllerRezeptGet = require('../controllers/Rezept/controllerRezeptGet');
var controllerRezeptPost = require('../controllers/Rezept/controllerRezeptPost');
require('../config/passport')(passport);


module.exports = function (app) {

//start [ View: index]->[Login]/[signup]
    app.get('/',controllerStart.start);

// login  [View :login]->  [View : listNewRezeptsHome]
    app.get('/login', controllerUserGet.login);
    app.post('/login',controllerUserPost.login);

//registrierung     [View :signup]->  [View : listNewRezeptsHome]
    app.get('/signup', controllerUserGet.signup);
    app.post('/signup', controllerUserPost.signup);

// Home page , [View : Home]
    app.get('/profile',  controllerUserGet.profile);


// logout  [View : Home]->[View : index]
    app.get('/logout',controllerUserGet.logout);

// add Form [View : Home]->[View :FormaAdRezept]->[View : listNewRezeptsHome]
    app.get('/addRezept', controllerRezeptGet.addRezept);
    app.post('/addRezept', controllerRezeptPost.addRezept);

//Find Rezept with param: KochZeit, Typ, Author, [View : Home]->[View :FormafindRezept]->[View : listAllRezepts]
    app.get('/findRezept', controllerRezeptGet.findRezeptForm);
    app.post('/findRezept', controllerRezeptPost.findRezept);

//All Rezept List, [View : Home]->[View : listAllRezepts]
    app.get('/rezeptList', controllerRezeptGet.rezeptList);

//All User List, [View : Home]->[View : lisAllUsers]
    app.get('/userAktivList', controllerUserGet.userAktivList);

// All Rezepts of user, [View : Home]->[View : listAllRezepts]
    app.post('/findUserRezept', controllerRezeptPost.findUserRezept);

//All Rezepts of Typ, [View : Home ]->listAllRezepts
    app.post('/findTypRezept', controllerRezeptPost.findTypRezept);

//All "Bewertungen" of Rezept. [View : tab_list]->[View : listRezeptBewertungen]
    app.get('/rezeptListewertungen/:id',controllerRezeptGet.rezeptListBewertungen);

    // Write "Bewertung" ,[View : View.ejs]->[View : listAllRezepts]
    app.post('/bewertung', controllerRezeptPost.bewertung);
    
// Choose Item [View : tab_list]->[View : View.ejs]
    app.get('/bearbeiten/:id', controllerRezeptGet.listId);

// Delete Item. [View : View.ejs]->[View : listAllRezepts]
    app.get('/delete/:id', controllerRezeptGet.delete);

// display Items edit form, [View : View.ejs]->[View : FormEditRezept]
    app.get('/edit/:id',controllerRezeptGet.editForm);

// save update in DB, [View : FormEditRezept]->[View : infoAfterEdit]
    app.post('/edit/:id', controllerRezeptPost.update);

   
    //  Choose User from users list  and find user's rezepts , [View : listAllUsers]->[View : listAllRezepts]
    app.get('/findUser/:name', controllerRezeptGet.userId)
};


